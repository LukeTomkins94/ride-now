package com.example.luket.ridenow;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Luket on 11/02/2018.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback {

    public GoogleMap mMap;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentManager fragment = getActivity().getSupportFragmentManager();

        View v = inflater.inflate(R.layout.fragment_map, container, false); // Displays new fragment on screen


        Object gm = null;

        if (gm == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map); // starts the google maps module
            mapFrag.getMapAsync(this);
        }


        return v;
    }

    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        LatLng Eindhoven = new LatLng(51.441, 5.469);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Eindhoven, 12.5f));




    }




}
